PROJECT_DIR=glasnost-xml
DIST_FILE=glasnost-xml.tar.gz

PYTHON_VERSION=2.3

FIND=find
PYGETTEXT=pygettext

dist-clean:

dist: dist-clean
	rm -rf $(PROJECT_DIR) && mkdir $(PROJECT_DIR)
	tar c   --exclude CVS --exclude .cvsignore --exclude '*.pyc' \
	        --exclude debian --exclude $(PROJECT_DIR) \
	        --exclude '.#*' . | (cd $(PROJECT_DIR) && tar x)
	rm -f $(PROJECT_DIR)/vhosts/*/sessions/[0-9]*.xml
	rm -f $(PROJECT_DIR)/vhosts/*/identities/[0-9]*.xml
	rm -f $(PROJECT_DIR)/vhosts/*/ballot-boxes/*/[0-9]*.xml
	#find $(PROJECT_DIR) -type l | xargs -r rm
	tar czf ../$(DIST_FILE) $(PROJECT_DIR)
	rm -rf $(PROJECT_DIR)

mo: po/fr.po
	-mkdir -p locale/fr/LC_MESSAGES
	msgfmt --statistics -c -v -o locale/fr/LC_MESSAGES/glasnost-xml.mo po/fr.po
	for F in `$(FIND) . -type f -name '*.xml.in'`; \
	do \
		intltool-merge -x po/ $$F `dirname $$F`/`basename $$F .in`; \
	done

translations:
	-cp po/fr.po po/fr.pox
	for F in `$(FIND) . -type f -name '*.xml.in'`; \
	do \
		intltool-extract --type=gettext/xml $$F; \
	done
	$(PYGETTEXT) -d po/messages -k N_ \
		`$(FIND) . -type f -name "*.py"` \
		`$(FIND) . -type f -name "*.xml.in.h"`
	msgmerge -o po/fr.po -D po fr.pox messages.pot
