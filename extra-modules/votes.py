# -*- coding: UTF-8 -*-


# Expression
# By: Frederic Peters <fpeters@entrouvert.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#
# Copyright (C) 2004 Entr'ouvert, Frederic Peters & Emmanuel Raviart
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


"""Votes Module"""


import dataholders
import elements
import environs
import namespaces
import stations


class Vote(things.Thing):
    def getContentAtXpath(self, xpath):
        nodes = self.evaluateXpath(xpath)
        if not nodes:
            return None
        return nodes[0].content


class VoteHolder(dataholders.XmlHolder):
    def save(self):
        super(VoteHolder, self).save()
        user = environs.getVar("user")
        user.setVoteToken(self.getParent().localId, self.localId)
        user.getDocument().save()


class Votes(things.Thing):
    pass
    #~ def walk1(self, uriPathFragments):
        #~ if uriPathFragments:
            #~ uriPathFragment = uriPathFragments[0]
            #~ # UriPathFragment is an election local id.
            #~ ballotBox = self.walkToLocation("/ballot-boxes/%s" % uriPathFragment)
            #~ vote = ballotBox.getUserVote()
            #~ voteHolder = vote.getDataHolder()
            #~ voteUriStep = steps.UriStep(
                #~ self.getDataHolder().resource.uriStep, uriPathFragment)
            #~ voteFileSystemStep = voteHolder.resource.fileSystemStep
            #~ voteFileSystemStep = steps.FileSystemStep(
                #~ voteFileSystemStep.previous, voteFileSystemStep.name)
            #~ voteFileSystemHierarchicalStep \
                #~ = voteHolder.resource.fileSystemHierarchicalStep
            #~ voteFileSystemHierarchicalStep \
                #~ = steps.FileSystemHierarchicalStep(
                    #~ voteFileSystemHierarchicalStep.getParent(),
                    #~ voteFileSystemHierarchicalStep.name)
            #~ voteResource = resources.LocalResource(
                #~ voteUriStep, voteFileSystemStep,
                #~ voteFileSystemHierarchicalStep)
            #~ node = voteHolder.node
            #~ voteHolder = dataholders.DataHolder(
                #~ voteResource, mimeType = voteHolder.mimeType,
                #~ outputMimeType = voteHolder.outputMimeType)
            #~ voteHolder.node = node.copyDoc(True)
            #~ voteHolder.setup()
            #~ return voteHolder.walk(uriPathFragments[1:])
        #~ return things.Thing.walk1(self, uriPathFragments)


elements.registerElement(
    namespaces.yep.uri, "vote", Vote, "http://www.entrouvert.org/expression/schemas/Vote.xsd",
    "http://www.entrouvert.org/expression/descriptions/Vote.xml", VoteHolder)
elements.registerElement(namespaces.yep.uri, "votes", Votes)
