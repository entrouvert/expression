# -*- coding: UTF-8 -*-


# Expression
# By: Frederic Peters <fpeters@entrouvert.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#
# Copyright (C) 2004 Entr'ouvert, Frederic Peters & Emmanuel Raviart
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


"""Inscriptions Module"""


import os

import libxml2

import dataholders
import elements
import emails
import environs
import locations
import logs
import namespaces
import sessions
import stations
import strings


class Inscription(things.Thing):
    language = "fr"

    def deleteSessionLocation(self):
        nodes = self.evaluateXpath("yep:session")
        if nodes:
            node = nodes[0]
            node.unlinkNode()
            node.freeNode()

    def getConnaissanceReglement(self):
        nodes = self.evaluateXpath("yep:connaissanceReglement")
        if not nodes:
            return None
        content = nodes[0].content
        if not content:
            return None
        return content not in ("0", "false") 

    def getCourriel(self):
        nodes = self.evaluateXpath("yep:courriel")
        if not nodes:
            return None
        #return unicode(nodes[0].content, "UTF-8")
        return nodes[0].content

    def getEmail(self):
        """Method needed to be used as an Identity."""
        return self.courriel

    def getEstInscriteSurPlateforme(self):
        nodes = self.evaluateXpath("yep:estInscriteSurPlateforme")
        if not nodes:
            return None
        content = nodes[0].content
        if not content:
            return None
        return content not in ("0", "false") 

    def getEstMembreAssociation(self):
        nodes = self.evaluateXpath("yep:estMembreAssociation")
        if not nodes:
            return None
        content = nodes[0].content
        if not content:
            return None
        return content not in ("0", "false") 

    def getFraisDossier(self):
        nodes = self.evaluateXpath("yep:fraisDossier")
        if not nodes:
            return None
        return nodes[0].content

    def getNomCollectivite(self):
        nodes = self.evaluateXpath("yep:nomCollectivite")
        if not nodes:
            return None
        #return unicode(nodes[0].content, "UTF-8")
        return nodes[0].content

    def getNomCorrespondant(self):
        nodes = self.evaluateXpath("yep:nomCorrespondant")
        if not nodes:
            return None
        #return unicode(nodes[0].content, "UTF-8")
        return nodes[0].content

    def getPassword(self):
        nodes = self.evaluateXpath("yep:password")
        if not nodes:
            return None
        #return unicode(nodes[0].content, "UTF-8")
        return nodes[0].content

    def getSessionLocation(self):
        nodes = self.evaluateXpath("yep:session/@src")
        if not nodes:
            return None
        return nodes[0].content

    def getSimpleLabel(self):
        return self.nomCollectivite

##     def modeAccessIsAuthorized(self, modeName):
##         if modeName == "identifiant":
##             # Only the user and the administrators can see the user password.
##             user = environs.getVar("user")
##             if user is None:
##                 return False
##             if strings.simplify(user.email) == self.getDataHolder().localId:
##                 return True
##             administrators = self.getAdministrators()
##             return administrators is not None and administrators.containsUser()
##         return things.Thing.modeAccessIsAuthorized(self, modeName)

    def setPassword(self, password):
        self.setElementContent("yep:password", password)

    def setSessionLocation(self, sessionLocation):
        self.setElementAttribute("yep:session", "src", sessionLocation)

    def submitToUrl(self, nextUrl = None):
        document = self.getDocument()
        description = self.getDescription()
        if description is None:
            if environs.getVar("debug"):
                raise Exception('Description for "%s" missing' % self)
            raise faults.PathNotFound(self.getUriInternPath())
        description.submit()
        alerts = {}
        if not self.connaissanceReglement:
            alerts["yep_connaissanceReglement"] = "Valeur manquante"
        if not self.courriel:
            alerts["yep_courriel"] = "Valeur manquante"
        if self.estInscriteSurPlateforme is None:
            alerts["yep_estInscriteSurPlateforme"] = "Valeur manquante"
        if self.estMembreAssociation is None:
            alerts["yep_estMembreAssociation"] = "Valeur manquante"
        if not self.fraisDossier:
            alerts["yep_fraisDossier"] = "Valeur manquante"
        if not self.nomCollectivite:
            alerts["yep_nomCollectivite"] = "Valeur manquante"
        if not self.nomCorrespondant:
            alerts["yep_nomCorrespondant"] = "Valeur manquante"
        if not "yep_courriel" in alerts:
            inscriptionExists = self.walkToLocation(
                "/inscriptions/%s" % strings.simplify(self.courriel), None,
                "exists")
            if document.localId.startswith("new-") and inscriptionExists:
                alerts["yep_courriel"] = "Adresse courriel déjà utilisée"

        if not alerts and document.localId.startswith("new-"):
            self.password = strings.makePassword(length = 6)

            subject = "Identifiant pour acceder au Label Villes Internet 2004"
            if self.fraisDossier == "0":
                texteFraisDossier = """\
Votre commune ou votre communauté est membre adhérent de l'association
Villes Internet, vous êtes donc exonéré de tout frais de dossier.
"""
            elif self.fraisDossier == "80":
                texteFraisDossier = """\
Votre commune ou votre communauté n'est pas membre adhérent de l'association
Villes Internet et compte un nombre d'habitants inférieur à 3000. Nous
attendons un règlement de 80 euros pour frais de dossiers avant le 30 mars
2004. Modalités de paiement : 
- Par chèque : à l'ordre de Villes Internet. 
Merci de renvoyer le chèque en n'oubliant pas d'inscrire au dos le numéro
d'identifiant ci-dessus à l'adresse suivante :
Villes Internet - Inscription Label 2004 - 45 rue de Belfort - 92400 COURBEVOIE 
- Par mandat administratif :
Société Générale Agence Neuilly-Mairie -
Code Banque : 30003 - Code Agence : 03908 Numéro de compte : 00037290562 -
Clé RIB : 77 - Titulaire du compte : Association Villes Internet
"""
            else:
                texteFraisDossier = """\
Votre commune ou votre communauté n'est pas membre adhérent de l'association
Villes Internet et compte un nombre d'habitants supérieur à 3000. Nous
attendons un règlement de 150 euros pour frais de dossiers avant le 30 mars
2004. Modalités de paiement : 
- Par chèque : à l'ordre de Villes Internet. 
Merci de renvoyer le chèque en n'oubliant pas d'inscrire au dos le numéro
d'identifiant ci-dessus à l'adresse suivante :
Villes Internet - Inscription Label 2004 - 45 rue de Belfort - 92400 COURBEVOIE 
- Par mandat administratif :
Société Générale Agence Neuilly-Mairie -
Code Banque : 30003 - Code Agence : 03908 Numéro de compte : 00037290562 -
Clé RIB : 77 - Titulaire du compte : Association Villes Internet
"""
            message = """\
Bonjour,

Votre inscription au Label Ville Internet 2004 a bien été prise en compte.
Merci de garder ce courriel récapitulatif, n'hésitez pas à l'imprimer !

Vos identifiants :
Courriel :      %(courriel)s
Identifiant :   %(password)s

Données concernant votre collectivité locale ou votre communauté de communes :
Nom :           %(nomCollectivite)s
Correspondant : %(nomCorrespondant)s

%(texteFraisDossier)s
Vous pouvez désormais remplir le questionnaire de votre commune à tout moment
à cette adresse : http://label.villes-internet.net
Ce questionnaire est divisé en 8 cahiers, que vous avez la possibilité de
remplir séparément, avant une validation finale, qui n'aura lieu qu'une
seule fois et rendra toutes vos réponses définitives et non-modifiables.

Pour toutes les questions relatives uniquement au contenu du questionnaire,
un service d'aide est disponible tous les mardis après-midi de 14h à 17h
par téléphone au : 05 63 71 56 12 ou écrire à : dess@villes-internet.net


Attention ! LA CLÔTURE DES INSCRIPTIONS AU LABEL 2004 EST LE : 

                        5 AVRIL 2004

Tout questionnaire non-validé définitivement à cette date-là ne sera pas
considéré comme participant à l'édition 2004 du Label Ville Internet.

Cordialement,

L'équipe Villes Internet
""" % {
                "courriel": self.courriel,
                "texteFraisDossier": texteFraisDossier,
                "nomCollectivite": self.nomCollectivite,
                "nomCorrespondant": self.nomCorrespondant,
                "password": self.password,
                }
            emailHolder = self.walk(["email"])
            email = emailHolder.getRootElement()
            try:
                email.send()
                #~ emails.sendMail(
                    #~ "coordination@villes-internet.net",
                    #~ self.courriel,
                    #~ subject,
                    #~ message)
            except: # TODO: tighter check
                logs.exception("SMTP send failed")
                alerts["yep_courriel"] = "Adresse de courriel erronée"

            if not alerts:
                subject = "Inscription au Label Villes Internet 2004"
                if self.estMembreAssociation:
                    adherente = "oui"
                else:
                    adherente = "non"
                if self.estInscriteSurPlateforme:
                    plateforme = "oui"
                else:
                    plateforme = "non"
                #~ message = """\
#~ Bonjour !

#~ Une nouvelle commune ou communauté s'est inscrite au Label !

#~ Nom : %(nomCollectivite)s
#~ Courriel : %(courriel)s
#~ Numéro d'identifiant : %(password)s
#~ Adhérente : %(adherente)s
#~ Frais de dossier : %(fraisDossier)s EUR
#~ Plateforme : %(plateforme)s
#~ Correspondant : %(nomCorrespondant)s

#~ @ bientôt !
#~ """ % {
                    #~ "adherente": adherente,
                    #~ "courriel": self.courriel,
                    #~ "fraisDossier": self.fraisDossier,
                    #~ "nomCollectivite": self.nomCollectivite,
                    #~ "nomCorrespondant": self.nomCorrespondant,
                    #~ "password": self.password,
                    #~ "plateforme": plateforme,
                    #~ }
                #~ try:
                    #~ emails.sendMail(
                        #~ "coordination@villes-internet.net",
                        #~ ["coordination@villes-internet.net",
                         #~ "label2004@entrouvert.com"],
                        #~ subject,
                        #~ message)
                #~ except: # TODO: tighter check
                    #~ logs.exception("SMTP send failed")

            if not alerts:
                document.setPermanentLocalId()
        if alerts:
            # Error in the login form.
            if document.localId.startswith("new-"):
                pageName = "new"
            else:
                pageName = "edit"
            environs.push(
                _level = "submitToUrl",
                pageName = pageName,
                #  FIXME: Now stored in modelContext.alerts.
                # currentAlerts = alerts,
                )
            try:
                self.output()
            finally:
                environs.pull(_level = "submitToUrl")
            return
        # document.save() is done below

        session = sessions.getOrCreateSession()
        session.authenticationMethod = "password"
        session.publishToken = True
        session.setAccountAbsolutePath(self.getAbsolutePath())
        self.sessionLocation = session.getUriInternPath()
        document.save()
        baseEnviron = environs.get(_level = "handleHttpCommand")
        baseEnviron.setVar("user", self)
        logs.debug("""Setting session user = "%s" at location "%s".""" % (
            self.simpleLabel, session.getAccountAbsolutePath()))
        nextUrl = document.getActionUri("identifiant")
        return environs.getVar('httpRequestHandler').outputRedirect(nextUrl)

    connaissanceReglement = property(getConnaissanceReglement)
    courriel = property(getCourriel)
    email = property(getEmail)
    fraisDossier = property(getFraisDossier)
    estInscriteSurPlateforme = property(getEstInscriteSurPlateforme)
    estMembreAssociation = property(getEstMembreAssociation)
    nomCollectivite = property(getNomCollectivite)
    nomCorrespondant = property(getNomCorrespondant)
    password = property(getPassword, setPassword)
    sessionLocation = property(
        getSessionLocation, setSessionLocation, deleteSessionLocation)
    simpleLabel = property(getSimpleLabel)


class InscriptionHolder(dataholders.XmlHolder):
    def deletePublicName(self):
        pass

    def getPublicName(self):
        email = self.getRootElement().email
        if email is None:
            return None
        # We replace " " by "-" instead of "_" because this is better for Google
        # indexing.
        publicName = strings.simplify(email)
        if self.isRootElder:
            publicName = os.path.join(self.getDirectoryAbsolutePath(), publicName)
        return publicName

    def setPublicName(self, publicName):
        pass

    publicName = property(getPublicName, setPublicName, deletePublicName)


elements.registerElement(
    namespaces.yep.uri, "inscription", Inscription,
    "http://www.entrouvert.org/expression/schemas/Inscription.xsd",
    "http://www.entrouvert.org/expression/descriptions/Inscription.xml", InscriptionHolder)
