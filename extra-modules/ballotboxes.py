# -*- coding: UTF-8 -*-


# Expression
# By: Frederic Peters <fpeters@entrouvert.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#
# Copyright (C) 2004 Entr'ouvert, Frederic Peters & Emmanuel Raviart
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


"""Ballot Boxes Module"""


import directories
import elements
import environs
import namespaces
import stations


class BallotBox(directories.Directory):
    def getUserVote(self):
        user = environs.getVar("user")
        if user is None:
            return None
        voteToken = user.getVoteToken(self.getDataHolder().localId)
        if voteToken is None:
            vote = None
        else:
            voteHolder = self.walkToLocation(self.getSubPathInternUri(
                voteToken))
            vote = voteHolder.getRootElement()
        if vote is None:
            vote = newVote(self, temporary = True)
        return vote


elements.registerElement(
    namespaces.yep.uri, "ballotBox", BallotBox,
    "http://www.entrouvert.org/expression/schemas/Directory.xsd",
    "http://www.entrouvert.org/expression/descriptions/Directory.xml")
