Example
=======

We want to access to http://localhost/dir/data.xml.

This file is on the computer at expression/vhosts/myvhost/dir/data.xml.

Order of calls:

Server.py:handleHttpCommand:
    - Initialize local web site variables.
    - Find the begin point of the walk of this virtual host, the *documentRoot*. This is the first *station*.
      Every *station* could be a *documentRoot*. This root *station* is titled *rootDataHolder* instance of
      *directoryHolder*.
    - Parses the URL.

rootDataHolder.walk(['dir', 'data.xml'], 'GET', None):
    - *rootDataHolder* is a *station*.
    - *walkToEvaluation* returns *self*.
    - *walkToContent* to select to correct walk to the content of the requested *station*.
        - Call *walkToItem* returns the *directoryHolder* of 'dir' and the left stations sequence.
        - returns the correct walk returns.
    - The returned *station* is not the same as actual *station*.
    - The sequence is not empty.
    - *isAccessAuthorized* for the specified command is true.
    
directoryHolder.walk(['data.xml'], 'GET', None):
    - *directoryHolder* is a *station*.
    - *walkToEvaluation* returns *self*.
    - *walkToContent* to select to correct walk to the content of the requested *station*.
        - Call *walkToItem* returns the *xmlHolder* of 'data.xml' and an empty sequence.
        - returns the correct walk returns.
    - The returned *station* is not the same as actual *station*.
    - The sequence is empty.
    - *isAccessAuthorized* for the specified command is true.

xmlHolder.walk([], 'GET', None):
    - *xmlHolder* is a *station*.
    - *walkToEvaluation* returns *self*.
    - *walkToContent*
        *walkToItem* return self.rootElement (*self* is the *xmlHolder* *station*).
    - The returned *station* is not the same as actual *station*.
    - The sequence is empty.
    - *isAccessAuthorized* for the specified command is true.

rootElement.walk([], 'GET', None):
    - *element* is a *station*.
    - *walkToEvaluation* returns *self*.
    - *walkToContent* return self and the empty sequence.
    - The returned *station* is the same as actual *station*.
    - The sequence is empty.
    - *isAccessAuthorized* for the specified command is true.
    - As 'GET' is the command, the *doHttpGet* method is called.

rootElement.doHttpGet()
    - Do nothing.

rootElement.doHttpGetStyled()
    - *generateHtmlDocument* generate a XHTML *document* object with the XML source in 'pre' tags.
    - Get the XSLT if applicable.

rootElement.outputHttpHtmlDocument()
    - Apply the XSLT if applicable.

httpRequestHandler.outputData(string, 'http://localhost/dir/images/data.jpg', mime/type, modificationDate)

The sequence is not empty and the returned *station* is instanciated,
