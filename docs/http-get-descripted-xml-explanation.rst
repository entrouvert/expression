data.xml
========

<myRoot>
 ...
 ...
 ...
</myRoot>

description
===========

A description is a link between a XML object and his representation.

Our description file is "myDescription.xml".

Request
=======

'data.xml'

Explanation
===========

- In this explanation, we are in the xmlHolder of data.xml.
- The *myRoot* tag is registered with our class *myRootElement*.
- The *myRoot* tag is registered with our description "myDescription.xml".
- The *walkToItem* and the *getDescription* method of our *station* class are not overridden (they use the
  description system.

xmlHolder.walk([], 'GET', None):
    - *xmlHolder* is a *station*.
    - *walkToEvaluation* returns *self*.
    - *walkToContent*

    xmlHolder.walkToItem([], 'GET', None):
        - call the next method.

    document.getRootElement():
        - get the *rootNode* (rootElement) of the libXML2 document.

    elements.newElement(*rootNode*):
        - Extract the node name and the namespace.
        - Lookup the registered corresponding python class, or get the default python class.
        - return the chosen class instance (in our case, the *myRootElement*).

    - The returned *station* is not the same as actual *station*.
    - The sequence is empty.
    - *isAccessAuthorized* for the specified command is true.

myRootElement.walk([], 'GET', None):
    - *myRootElement* is a *station*.
    - *walkToEvaluation* returns *self*.
    - *walkToContent*
        - *walkToItem*([], 'GET', None):
            - return the *myDescription* *description* instance.
    - The returned *station* is not the same as actual *station*.
    - The sequence is empty.
    - *isAccessAuthorized* for the specified command is true.

myDescription.walk([], 'GET', None):
    - *myDescription* is a *station*.
    - *walkToEvaluation* returns *self*.
    - *walkToContent*
        - *walkToItem*([], 'GET', None):
            - return self.
    - The returned *station* is the same as actual *station*.
    - The sequence is empty.
    - *isAccessAuthorized* for the specified command is true.
    - As 'GET' is the command, the *doHttpGet* method is called.

myDescription.doHttpGet()
    - Do nothing.

myDescription.doHttpGetStyled()
    - *generateHtmlDocument* generate a XHTML *document*.
        - Apply XFORM to the selected file.
    - Get the XSLT if applicable.

myDescription.outputHttpHtmlDocument()
    - Apply the XSLT if applicable.


httpRequestHandler.outputData(string, '.../data.xml/action', mime/type, modificationDate)


Notes
=====

In order to access to more than one level, we have to code it for the moment.
