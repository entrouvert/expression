Expression works by *station*, a *station* is a representation of the step stage of a walk,
a "stop in a ride".

Station
=======

A station can match a directory, a file, an XML node ...

DataHolder
==========

    - A Dataholder is a file.

    - A StaticDataholder is a file that is just passed through to the caller, such as an image file.

    - A file shown as is, without any traitment.

    - An XMLHolder is a file that contains an XML document.

    - directory holder:
        Surpass xmlHolder with filename/index.xml

    - shema holder:
        Surpass with .xsd extension.

Step
====

When *Expression* goes from *station* to *station*, sometime, it have to go back to the way it passed.
There is no parent attribute.

A *station* can be accessed from 3 way:
    - URI way, by URL,
    - internal way, by *stations*, one by one,
    - hierarchical way, by tree position (path rewinded to find back the rights).

The *internals ways* are nodes pointing to a *station*.

Element
=======

This is a XML element node.

Document
========

A document is distinct from an Element, as it is so in libxml2.
It is more than just a root element.


Walk methods
============

The *walk* asks for a path, a command and an instruction.

The *walk* it is the principal function using the path to browse. The stages of a *walk* are similar.

The walk method calls walkToEvalutation, walkToContent or walkToLocation. The walk method is recursive but none of the 3 others are.

A node can have actions, or items. The items are tested, then the action, and if this is no items nor action, this is no content.


walkToEvalutation
-----------------

Used when there is no need for a pre-process, file returned as is or executed.
To execute a python file, for example.
Also used to implement aliases.

walkToContent
-------------

Handles an item or an action (in this order).
An item is a child element (such as a file in a directory)
Actions are names registered to match a handler method.
walkToContent calls walkToItem, then (upon failure to find the item), walkToAction.

walkToLocation
--------------

Like a fast goto.
walkToLocation(url, HTTPCommand, instruction)
    url is the absolute URL to a ressource
    HTTPCommand is either
        - None (you get the object itself)
        - Get, Post, Put, ...
    Instruction is either
        - exists
        - isAuthorised
        - existsAndIsAuthorised


Groups
======

Objects containing identities or groups.
We can ask an identified identity with a certain method.
*everybody* is a special group containing everybody, and the non authenticated people too.

In each xmlHolder, we can have a *users* element that identifies the users who can acces the file. We also find it in each *modes* (edit, etc...) to define who can access it.
e.g.
<users><antoine><nicolas><seb>	// antoine, nicolas, seb can access this file
   <mode name="edit">
      <users><antoine></users>	// But only Antoine can access the edit mode
   </mode>
</users>
