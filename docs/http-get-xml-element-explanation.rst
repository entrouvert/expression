data.xml
========

<myRoot>
 ...
 ...
 ...
</myRoot>

Request
=======

'data.xml/action'

Explanation
===========

*In this explanation, we are in the xmlHolder of data.xml. The myRoot tag is registered with our class myRootElement.*

xmlHolder.walk(['action'], 'GET', None):
    - *xmlHolder* is a *station*.
    - *walkToEvaluation* returns *self*.
    - *walkToContent*

    xmlHolder.walkToItem(['action'], 'GET', None):
        - call the next method.

    document.getRootElement():
        - get the *rootNode* (rootElement) of the libXML2 document.

    elements.newElement(*rootNode*):
        - Extract the node name and the namespace.
        - Lookup the registered corresponding python class, or get the default python class.
        - return the chosen class instance (in our case, the *myRootElement*).

    - The returned *station* is not the same as actual *station*.
    - The sequence is empty.
    - *isAccessAuthorized* for the specified command is true.

myRootElement.walk(['action'], 'GET', None):
    - *myRootElement* is a *station*.
    - *walkToEvaluation* returns *self*.
    - *walkToContent*

    - *walkToItem*(['action'], 'GET', None):
        - do nothing.
    - *walkToAction*
        - We have overridden the method.

httpRequestHandler.outputData(string, '.../data.xml/action', mime/type, modificationDate)


Notes
=====

In order to access to more than one level, we have to code it for the moment.
