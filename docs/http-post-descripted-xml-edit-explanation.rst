data.xml
========

<article>
 ...
 ...
 ...
</article>

description
===========

A description is a link between a XML object and his representation.

Our description file is "Article.xml".

Request
=======

POST 'data.xml/edit'

Explanation
===========

- In this explanation, we are in the xmlHolder of data.xml.
- The *article* tag is a default *expression* root element, the "Article.xml" description file and the
  *element* class used is the default scheme.

xmlHolder.walk(['edit'], 'POST', None):
    - *xmlHolder* is a *station*.
    - *walkToEvaluation* returns *self*.
    - *walkToContent*
        xmlHolder.walkToItem([], 'POST', None):
            - call the next method.

        document.getRootElement():
            - get the *rootNode* (rootElement) of the libXML2 document.

        elements.newElement(*rootNode*):
            - Extract the node name and the namespace.
            - Lookup the registered corresponding python class, or get the default python class.
            - return the default class instance (in our case, *Element*).

    - The returned *station* is not the same as actual *station*.
    - The sequence is not empty.
    - *isAccessAuthorized* for the specified command is true.

Element.walk(['edit'], 'POST', None):
    - *Element* is a *station*.
    - *walkToEvaluation* returns *self*.
    - *walkToContent*
        - *walkToItem*(['edit'], 'POST', None):
            - return the *description* instance.
    - The returned *station* is not the same as actual *station*.
    - The sequence is not empty.
    - *isAccessAuthorized* for the specified command is true.

description.walk(['edit'], 'POST', None):
    - *description* is a *station*.
    - *walkToEvaluation* returns *self*.
    - *walkToContent*
        - *walkToItem*(['edit'], 'POST', None):
            - Look for a *page* node in the description named "edit".
            - return the *Page* instance.
    - The returned *station* is not the same as actual *station*.
    - The sequence is empty.
    - *isAccessAuthorized* for the specified command is true.

Page.walk([], 'POST', None):
    - *Page* is a *station*.
    - *walkToEvaluation* returns *self*.
    - *walkToContent*
        - *walkToItem*([], 'POST', None):
            - return self            
    - The returned *station* is the same as actual *station*.
    - The sequence is empty.
    - *isAccessAuthorized* for the specified command is true.
    - As 'POST' is the command, the *doHttpPost* method is called.

Page.doHttpPost():
    - return the next function return.

Page.prototype.submitValidateAndProcessContext():
    - Apply the posted HTML form to the Page object with or without errors.
    - Save the object to his attributed file path.
    - Redirect to the same URL in 'GET'.

httpRequestHandler.outputData(string, '.../data.xml/action', mime/type, modificationDate)


Notes
=====

In order to access to more than one level, we have to code it for the moment.
