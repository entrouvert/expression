Example
=======

We want to access to http://localhost/dir/images/data.jpg.

This file is on the computer at expression/vhosts/myvhost/dir/images/data.jpg.

Order of calls:

Server.py:handleHttpCommand:
    - Initialize local web site variables.
    - Find the begin point of the walk of this virtual host, the *documentRoot*. This is the first *station*.
      Every *station* could be a *documentRoot*. This root *station* is titled *rootDataHolder* instance of
      *directoryHolder*.
    - Parses the URL.

rootDataHolder.walk(['dir', 'images', 'data.jpg'], 'GET', None):
    - *rootDataHolder* is a *station*.
    - *walkToEvaluation* returns *self*.
    - *walkToContent* to select to correct walk to the content of the requested *station*.
        - Call *walkToItem* returns the *directoryHolder* of 'dir' and the left stations sequence.
        - returns the correct walk returns.
    - The returned *station* is not the same as actual *station*.
    - The sequence is not empty.
    - *isAccessAuthorized* for the specified command is true.

directoryHolder.walk(['images', 'data.jpg'], 'GET', None):
    - *directoryHolder* is a *station*.
    - *walkToEvaluation* returns *self*.
    - *walkToContent* to select to correct walk to the content of the requested *station*.
        - Call *walkToItem* returns the *directoryHolder* of 'images' and the left stations sequence.
        - returns the correct walk returns.
    - The returned *station* is not the same as actual *station*.
    - The sequence is not empty.
    - *isAccessAuthorized* for the specified command is true.
    
directoryHolder.walk(['data.jpg'], 'GET', None):
    - *directoryHolder* is a *station*.
    - *walkToEvaluation* returns *self*.
    - *walkToContent* to select to correct walk to the content of the requested *station*.
        - Call *walkToItem* returns the *imageHolder* of 'data.jpg' and an empty sequence.
        - returns the correct walk returns.
    - The returned *station* is not the same as actual *station*.
    - The sequence is empty.
    - *isAccessAuthorized* for the specified command is true.

imageHolder.walk([], 'GET', None):
    - *imageHolder* is a *station*.
    - *walkToEvaluation* returns *self*.
    - *walkToContent* return self and the empty sequence.
    - The returned *station* is the same as actual *station*.
    - The sequence is empty.
    - *isAccessAuthorized* for the specified command is true.
    - As 'GET' is the command, the *doHttpGet* method is called.

imageHolder.doHttpGet()
    - Do nothing.

imageHolder.doHttpGetSource()
    - Do nothing.

imageHolder.outputHttpSource()
    - Open the image file descriptor.

httpRequestHandler.outputData(imageFD, 'http://localhost/dir/images/data.jpg', mime/type, modificationDate)

The sequence is not empty and the returned *station* is instanciated,
    
