<?xml version="1.0"?>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:yep="http://www.entrouvert.org/namespaces/expression/0.0"
  xmlns:html="http://www.w3.org/1999/xhtml"
  xsl:exclude-result-prefixes="yep html">
 <xsl:output method="xml" encoding="UTF-8" indent="yes"
   doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
   doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>

 <xsl:template match="/*">
  <html xmlns="http://www.w3.org/1999/xhtml">
   <head>
    <title>Formulaires</title>
    <link rel="stylesheet" type="text/css" href="/css/form.css" />
    <!-- calendar stylesheet -->
    <link rel="stylesheet" type="text/css" media="all"
          href="/javascript/jscalendar/calendar-win2k-cold-1.css"
          title="win2k-cold-1" />
    <!-- main calendar program -->
    <script type="text/javascript"
            src="/javascript/jscalendar/calendar.js"></script>
    <!-- language for the calendar -->
    <script type="text/javascript"
            src="/javascript/jscalendar/lang/calendar-fr.js"></script>
    <!-- the following script defines the Calendar.setup helper function, which 
         makes adding a calendar a matter of 1 or 2 lines of code. -->
    <script type="text/javascript"
            src="/javascript/jscalendar/calendar-setup.js"></script>

   </head>
   <body>
    <div id="main">
     <div class="banner">
      <h1 id="logo"><a href="/">Service de formulaires de Vandoeuvre</a></h1>
      <div id="owner">
       <div id="vandoeuvre">
        <a href="#" title="Ville de Vandoeuvre"><span>Ville de Vandoeuvre</span></a>
       </div>
       <div id="nancy">
        <a href="#" title="Grand Nancy"><span>Grand Nancy</span></a>
       </div>
      </div>
     </div>
 
     <div class="intro">
      <span class="intro">
       Expérimentation dans le cadre de la Carte de Vie Quotidienne (CVQ)
      </span>
     </div>
 
     <div class="explanation">
      <p>
      	Ce service est encore en phase d'expérimentation, n'hésitez pas à faire
	part de vos commentaires à la Mairie à l'adresse suivante:
	<a href="mailto:mairie-vandoeuvre@mairie-vandoeuvre.fr">mairie-vandoeuvre@mairie-vandoeuvre.fr</a>.
      </p>
     </div>
 
     <div id="main-content">
      <xsl:apply-templates select="html:body/*"/>
     </div>
     <div id="footer">
      <xsl:choose>
       <xsl:when test="yep:user-authenticated()">
        <a class="button" href="{yep:url('/liberty-alliance/logout', 'nextUrl', yep:url-intern-path())}">Se déconnecter</a>
       </xsl:when>
       <xsl:otherwise>
        <a class="button" href="{yep:url('/liberty-alliance/login', 'nextUrl', yep:url-intern-path())}">S'identifier</a>
       </xsl:otherwise>
      </xsl:choose>

      <div id="lasso">
       <a href="http://glasnost.entrouvert.org/lasso" title="LASSO"><span>Lasso</span></a>
      </div>
     </div>
    </div>
   </body>
  </html>
 </xsl:template>

 <xsl:template match="@href">
  <xsl:attribute name="href"> <xsl:value-of select="yep:url(string(.))"/> </xsl:attribute>
 </xsl:template>

 <xsl:template match="@src">
  <xsl:attribute name="src"> <xsl:value-of select="yep:url(string(.))"/> </xsl:attribute>
 </xsl:template>

 <xsl:template match="node()|@*">
  <xsl:copy>
   <xsl:apply-templates select="node()|@*"/>
  </xsl:copy>
 </xsl:template>

</xsl:stylesheet>
