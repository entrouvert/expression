<?xml version="1.0"?>
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:yep="http://www.entrouvert.org/namespaces/expression/0.0"
    xmlns:html="http://www.w3.org/1999/xhtml"
    xsl:exclude-result-prefixes="yep html">
 <xsl:output method="xml" encoding="UTF-8" indent="yes"
     doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
     doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>

 <xsl:template match="/*">
  <html xmlns="http://www.w3.org/1999/xhtml">
   <head>
    <title><xsl:value-of select="yep:site-title()"/></title>
    <xsl:apply-templates select="html:head/html:link"/>
    <xsl:apply-templates select="html:head/html:meta"/>
    <xsl:apply-templates select="html:head/html:script"/>

   </head>
   <body>
    <h1 id="logo">Expression</h1>
    <div id="main-content">
     <xsl:apply-templates select="html:body/*"/>
    </div>
   </body>
  </html>
 </xsl:template>

 <xsl:template match="@href">
  <xsl:attribute name="href"> <xsl:value-of select="yep:url(string(.))"/> </xsl:attribute>
 </xsl:template>

 <xsl:template match="@src">
  <xsl:attribute name="src"> <xsl:value-of select="yep:url(string(.))"/> </xsl:attribute>
 </xsl:template>

 <xsl:template match="node()|@*">
  <xsl:copy>
   <xsl:apply-templates select="node()|@*"/>
  </xsl:copy>
 </xsl:template>

</xsl:stylesheet>
