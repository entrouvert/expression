# -*- coding: UTF-8 -*-


# Expression
# By: Frederic Peters <fpeters@entrouvert.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#
# Copyright (C) 2004 Entr'ouvert, Frederic Peters & Emmanuel Raviart
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


"""Logout"""


import environs
import locations


session = environs.getVar("session")
user = environs.getVar("user")
if session is not None and user is not None:
    user.deleteSessionToken()
    user.getDocument().save()
    baseEnviron = environs.get(_level = "handleHttpCommand")
    baseEnviron.setVar("user", None)
    session.getDataHolder().destroy()
    baseEnviron.setVar("session", None)
submission = environs.getVar("submission")
nextUrl = submission.getField("nextUrl")
if not nextUrl:
    nextUrl = "/"
nextUrl = locations.removeSessionFromUrl(nextUrl)
environs.getVar("httpRequestHandler").outputRedirect(nextUrl)
