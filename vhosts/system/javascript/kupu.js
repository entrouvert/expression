function addEvent(obj, evType, fn) {
    /* adds an eventListener for browsers which support it.
       Derived from snippet by Scott Andrew */
    if (obj.addEventListener) {
        obj.addEventListener(evType, fn, true);
        return true;
    }
    if (obj.attachEvent)
        return obj.attachEvent("on"+evType, fn);
    return false;
}

var kupu = null;
var kupuui = null;
function startKupu() {
    var frame = document.getElementById('kupu-editor'); 
    kupu = initKupu(frame); 
    kupuui = kupu.getTool('ui'); 
    kupu.initialize();
};

addEvent(window, "load", startKupu);
