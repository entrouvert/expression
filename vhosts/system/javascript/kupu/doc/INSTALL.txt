Installing Kupu
===============

  Written by: Guido Wesdorp
  Email: guido@infrae.com
  Valid for: Kupu 1.0.3


Installation requirements
-------------------------

- A webserver that can handle some form of server-side processing, and 
  preferrably also HTTP PUT requests (POST is also supported, see 'PUT or
  POST' below).


Installation
------------

1. Basic install on a 'plain' webserver (Apache, IIS, etc.) (quick install
    guide)

  Unpack the tarball into some directory that is reachable from
  HTTP. Copy kupu.html, change the 'src' and 'dst' attributes on the
  iframe so they point to the source of the editable object and the
  destination to save to and modify the paths of all 'script' and
  'link' tags so they point to the files in kupu/common.

2. Configuration and customization

  The default kupu.html is quite sober: it serves as an example or
  base document rather then as an out-of-the-box webpage. To configure
  Kupu you can set some attribute values on the iframe, change the CSS
  or override initKupu() (the initialization function). The latter is
  only necessary for larger customizations, simpler ones (like
  removing a tool from the UI or changing a button image) will usually
  only mean overriding the CSS. For more information about
  customization and configuration see CUSTOMIZING.txt.

3. Adding dynamics
  
  To use the kupu.html file for editing multiple files, you will
  probably want to make the 'src' and 'dst' attributes get filled from
  script (SSI, PHP, ASP, etc.). Kupu will load it's content from the
  URL in the 'src' attribute (actually it lets the browser do that,
  it's the default behaviour of an iframe) and will send the results
  to the URL in the 'dst' attribute (this obviously is done by Kupu
  rather than by the browser).  Although this sort of thing isn't hard
  to accomplish, writing dynamic web applications is beyond the scope
  of this document, partially because there are many different
  platforms on which Kupu will run (and as many different server-side
  scripting languages and technologies). There is, however, plenty of
  documentation about this subject available on-line.

4. PUT or POST

  Kupu will by default send its data to the server the HTTP PUT
  request method. Using PUT rather than POST has some advantages: Kupu
  can make asynchronous PUT requests (currently async POST is not
  implemented) so when a user saves the page doesn't have to be
  reloaded, and since PUT is invented for storing full documents and
  Kupu edits full documents, it is the most logical and elegant
  request method to use.

  If for some reason PUT is not available on your platform, Kupu can
  also be used inside an HTML form to participate in a POST
  situation. This way Kupu behaves like it is a field in a form, when
  the form gets submitted Kupu will add a hidden form field to the
  form and place it's contents in the field, so it is part of the
  request body when the form is submitted. To accomplish this, a
  method called 'prepareForm' should be called just before the form is
  submitted. For an example of how to use this method, see
  kupuform.html (which can also be used as the basis for your Kupu
  editor page, like kupu.html).
  
  Setting up a webserver is or explaining the details about writing a script 
  that handles POST or PUT requests is out of scope for this document but 
  there should be plenty documentation available on the internet. 
 
5. Zope

  Instructions for installing Kupu on Zope can be found in
  INSTALL_ZOPE.txt.
 
6. Installation problems

  If you have installation problems, see
  http://kupu.sourceforge.net/resources.html for a list of possible
  places to ask questions (mailinglist, IRC channel) or send an email
  to guido@infrae.com.
