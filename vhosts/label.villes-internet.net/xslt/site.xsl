<?xml version="1.0"?>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:yep="http://www.entrouvert.org/namespaces/expression/0.0"
  xmlns:html="http://www.w3.org/1999/xhtml"
  xsl:exclude-result-prefixes="yep">
 <xsl:output method="xml" encoding="UTF-8" indent="yes"
   doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
   doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>

 <xsl:template match="/*">
  <html xmlns="http://www.w3.org/1999/xhtml">
   <head>
    <title>Villes Internet - Promouvoir l'internet citoyen</title>
    <link rel="stylesheet" type="text/css" href="/css/villes-internet.css" />

    <!-- calendar stylesheet -->
    <link rel="stylesheet" type="text/css" media="all"
          href="/javascript/jscalendar/calendar-win2k-cold-1.css"
          title="win2k-cold-1" />
    <!-- main calendar program -->
    <script type="text/javascript"
            src="/javascript/jscalendar/calendar.js"></script>
    <!-- language for the calendar -->
    <script type="text/javascript"
            src="/javascript/jscalendar/lang/calendar-fr.js"></script>
    <!-- the following script defines the Calendar.setup helper function, which 
         makes adding a calendar a matter of 1 or 2 lines of code. -->
    <script type="text/javascript"
            src="/javascript/jscalendar/calendar-setup.js"></script>

   </head>
   <body>
    <div id="header">
     <span>Villes Internet</span>
    </div>
    <div id="main-content">
     <xsl:apply-templates select="html:body/*"/>
    </div>
   </body>
  </html>
 </xsl:template>

 <xsl:template match="@href">
  <xsl:attribute name="href"> <xsl:value-of select="yep:url(string(.))"/> </xsl:attribute>
 </xsl:template>

 <xsl:template match="@src">
  <xsl:attribute name="src"> <xsl:value-of select="yep:url(string(.))"/> </xsl:attribute>
 </xsl:template>

 <xsl:template match="node()|@*">
  <xsl:copy>
   <xsl:apply-templates select="node()|@*"/>
  </xsl:copy>
 </xsl:template>

</xsl:stylesheet>
