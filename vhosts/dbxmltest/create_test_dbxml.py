#!/usr/bin/python
# -*- coding: utf-8 -*-

import dbxml

books = {
    'short' : '<short />',
    'oui oui' : '<book><title>Oui oui à la plage</title></book>',
    'article' : '''<?xml version="1.0"?>
<article xmlns="http://www.entrouvert.org/namespaces/expression/0.0">
  <title>This is the article title</title>
  <body>This is the article body.&#13;
Just for testing.</body>
</article>''',
    'article2' : '''<?xml version="1.0"?>
<article xmlns="http://www.entrouvert.org/namespaces/expression/0.0">
  <title>Another article</title>
  <body>Bla bla bla.</body>
</article>''',
}

manager = dbxml.XmlManager()
updateContext = manager.createUpdateContext()
container = manager.createContainer("test.dbxml")

for name in books.keys():
    container.putDocument(name, books[name], updateContext)

