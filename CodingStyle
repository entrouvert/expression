Expression coding style
=======================

Indentation
-----------

Tabs in Python files are four spaces.

Tabs in XML files are one space (as if XML was not enough unreadable). But
indentation inside an XML element (ie between two attributes) are four spaces.


Breaking long lines
-------------------

The limit on the length of lines is 100 characters.  This is arbitrary and over
eighty.  Ditch your VT100.


Naming
------

As Linus said:
  C is a Spartan language, and so should your naming be.  Unlike Modula-2
  and Pascal programmers, C programmers do not use cute names like
  ThisVariableIsATemporaryCounter.  A C programmer would call that
  variable "tmp", which is much easier to write, and not the least more
  difficult to understand.

Forget it.  Variables must be over fifteen characters.  As a rule of thumb, if
you can remember the whole name of the variable, it is too short.

And they are camelCased.

So:
  serverIpAddressAndPortCouple and NOT serverAddress (or, God forbid, address)

XML element names are also camelCased: <requestForm/>.

Python class names and XML element type names have the first letter of each word capitalized: "Card", "RequestForm".
"XML" is a word. Therefore: class XmlHolder (not XMLHolder)


End of file
-----------

Files are terminated by *one* CR (0x0a) character; not two.  This is the only
place where you MUST pay attention to file size.


Sorting
-------

Imports are sorted alphabetically, with:
  * Python standard modules first
  * Non standard modules second
  * Expression modules last

Classes within one file are sorted alphabetically.

Attributes are placed before methods and sorted alphabetically.
All attributes must be explicitly declared (be it an instance or
a class attribute).

Methods within a class are sorted alphabetically.


Quotes
------

Enclose strings using double quotes, not single quotes.


XSL Functions
-------------

They are consistent with standard XSL functions; lower-case, dashes and not
starting with "get-".


Disagreement
------------

You may disagree with those rules.  As a matter of fact, I do.


-- 
Frederic Peters, Sat, 22 May 2004 17:00:00 +0200
