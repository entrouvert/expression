# -*- coding: UTF-8 -*-


# Expression
# By: Frederic Peters <fpeters@entrouvert.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#
# Copyright (C) 2004 Entr'ouvert, Frederic Peters & Emmanuel Raviart
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


"""Liberty Alliance Responses Module"""


import os

import expression.core.dataholders as dataholders
import expression.core.elements as elements
import expression.core.namespaces as namespaces
import expression.core.strings as strings
import expression.core.things as things


class LibertyAllianceResponse(things.Thing):
    def deleteArtifact(self):
        nodes = self.evaluateXpath("yep:artifact")
        if nodes:
            node = nodes[0]
            node.unlinkNode()
            node.freeNode()

    def deleteResponseDump(self):
        nodes = self.evaluateXpath("yep:responseDump")
        if nodes:
            node = nodes[0]
            node.unlinkNode()
            node.freeNode()

    def getArtifact(self):
        nodes = self.evaluateXpath("yep:artifact")
        if nodes:
            return nodes[0].content
        else:
            return None

    def getResponseDump(self):
        nodes = self.evaluateXpath("yep:responseDump")
        if nodes:
            return nodes[0].content
        else:
            return None

    def setArtifact(self, artifact):
        self.setElementContent("yep:artifact", artifact)

    def setResponseDump(self, responseDump):
        self.setElementContent("yep:responseDump", responseDump)


class LibertyAllianceResponseHolder(dataholders.XmlHolder):
    def deletePublicName(self):
        pass

    def getPublicName(self):
        publicName = self.getRootElement().getArtifact()
        if publicName is None:
            return None
        # Remove "/" from artifact (base64 includes the "/").
        publicName = strings.simplify(publicName)
        if self.isRootElder:
            publicName = os.path.join(self.getDirectoryAbsolutePath(), publicName)
        return publicName

    def setPublicName(self, publicName):
        pass

    publicName = property(getPublicName, setPublicName, deletePublicName)


elements.registerElement(
    namespaces.yep.uri, "libertyAllianceResponse", LibertyAllianceResponse,
    holderClass = LibertyAllianceResponseHolder)
