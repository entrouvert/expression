# -*- coding: UTF-8 -*-


# Expression
# By: Frederic Peters <fpeters@entrouvert.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#
# Copyright (C) 2004 Entr'ouvert, Frederic Peters & Emmanuel Raviart
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


"""Persons Module"""


import expression.core.elements as elements
import expression.core.namespaces as namespaces
import expression.core.things as things


class Person(things.Thing):

    def getEmail(self):
        nodes = self.evaluateXpath("yep:email")
        if not nodes:
            return None
        return nodes[0].content

    def getFullName(self):
        nodes = self.evaluateXpath("yep:fullName")
        if not nodes:
            return None
        return nodes[0].content

    def getLanguage(self):
        nodes = self.evaluateXpath("yep:language")
        if not nodes:
            return None
        return nodes[0].content

    def getSimpleLabel(self):
        return self.fullName

    def setFullName(self, fullName):
        self.setElementContent("yep:fullName", fullName)

    fullName = property(getFullName, setFullName)
    simpleLabel = property(getSimpleLabel)


elements.registerElement(
    namespaces.yep.uri, "person", Person,
    "http://www.entrouvert.org/expression/schemas/User.xsd",
    "http://www.entrouvert.org/expression/descriptions/Person.xml")
