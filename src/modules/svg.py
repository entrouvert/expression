# -*- coding: UTF-8 -*-


# Expression
# By: Frederic Peters <fpeters@entrouvert.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#     Sébastien Ducoulombier <sebastien.ducoulombier@lesdeveloppementsdurables.com>
#
# Copyright (C) 2004 Entr'ouvert, Frederic Peters & Emmanuel Raviart
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


"""SVG Module"""


import os
import tempfile

import expression.core.elements as elements
import expression.core.environs as environs
import expression.core.dataholders as dataholders
import expression.core.logs as logs
import expression.core.namespaces as namespaces
import expression.core.things as things


class SVGHolder(dataholders.StaticDataHolder):
    """ An Xml document file that gets returned as a PNG file. """

    def getDataFile(self):
        """ Renders the PNG file, and returns its content.
        """
        filename = self.getAbsolutePathName()
        cachefile = filename + ".cache"
        #FIXME : test whether the filename is more recent than the cache file. If so, return the cache file, else regenerate it first.
        os.system("svg2png %s %s" % (filename, cachefile))
        return open(cachefile)


class SVG(things.Thing):
    """ Element svg that contains an SVG document to be rendered.
    """
    def doHttpGet(self):
        """ Renders the PNG file and HTTP-returns its content.
        """
        file = tempfile.NamedTemporaryFile()
        file.write("""<?xml version="1.0"?>\n%s""" % self.serialize())
        file.flush()
        outfile = file.name + ".temp"
        os.system("inkscape -e %s -z %s" % (outfile, file.name))
        environs.getVar("httpRequestHandler").outputData(open(outfile).read(), mimeType = "image/png")
        os.unlink(outfile)


elements.registerElement(namespaces.svg.uri, "svg", SVG)
