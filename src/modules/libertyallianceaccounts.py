# -*- coding: UTF-8 -*-


# Expression
# By: Frederic Peters <fpeters@entrouvert.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#
# Copyright (C) 2004 Entr'ouvert, Frederic Peters & Emmanuel Raviart
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


"""Liberty Alliance Accounts Module"""


import errno
import os

import expression.core.dataholders as dataholders
import expression.core.elements as elements
import expression.core.filesystems as filesystems
import expression.core.namespaces as namespaces
import expression.core.strings as strings
import expression.core.things as things


class LibertyAllianceAccount(things.Thing):
    _user = None

    def deleteAuthenticationMethod(self):
        nodes = self.evaluateXpath("yep:authenticationMethod")
        if nodes:
            node = nodes[0]
            node.unlinkNode()
            node.freeNode()

    def deleteNameIdentifier(self):
        nodes = self.evaluateXpath("yep:nameIdentifier")
        if nodes:
            node = nodes[0]
            node.unlinkNode()
            node.freeNode()

    def deleteUserContextDump(self):
        nodes = self.evaluateXpath("yep:userContextDump")
        if nodes:
            node = nodes[0]
            node.unlinkNode()
            node.freeNode()

    def deleteUserPath(self):
        nodes = self.evaluateXpath("yep:user")
        if nodes:
            node = nodes[0]
            node.unlinkNode()
            node.freeNode()

    def getAuthenticationMethod(self):
        nodes = self.evaluateXpath("yep:authenticationMethod")
        if nodes:
            return nodes[0].content
        else:
            return None

    def getNameIdentifier(self):
        nodes = self.evaluateXpath("yep:nameIdentifier")
        if nodes:
            return nodes[0].content
        else:
            return None

    def getSession(self):
        user = self.getUser()
        if user is None:
            return None
        return user.getSession()

    def getUser(self):
        if self._user is None:
            userAbsolutePath = self.getUserAbsolutePath()
            if userAbsolutePath:
                try:
                    userHolder = dataholders.DataHolder(
                        pathFragment = userAbsolutePath, mimeType = "text/xml", isRootElder = True,
                        containedFileSystem = filesystems.PartialFileSystem(userAbsolutePath))
                except IOError, error:
                    if error.errno == errno.ENOENT:
                        logs.debug("""User at path "%s" doesn't exist.""" % userAbsolutePath)
                        self._user = "none"
                    else:
                        raise
                else:
                    # Some times, users are stored in independant files; some times they are
                    # embedded inside accounts.
                    accountOrUser = userHolder.getRootElement()
                    self._user = accountOrUser.getUser()
            else:
                userNodes = self.evaluateXpath("yep:user")
                if userNodes:
                    self._user = elements.newElement(userNodes[0], previous = self, owner = self)
                else:
                    self._user = "none"
        if self._user == "none":
            return None
        return self._user

    def getUserAbsolutePath(self):
        nodes = self.evaluateXpath("yep:user/@src")
        if nodes:
            userPath = nodes[0].content
            return self.convertPathToAbsolute(userPath)
        else:
            return None

    def getUserContextDump(self):
        nodes = self.evaluateXpath("yep:userContextDump")
        if nodes:
            return nodes[0].content
        else:
            return None

    def setAuthenticationMethod(self, authenticationMethod):
        self.setElementContent("yep:authenticationMethod", authenticationMethod)

    def setNameIdentifier(self, nameIdentifier):
        self.setElementContent("yep:nameIdentifier", nameIdentifier)

    def setUserAbsolutePath(self, userAbsolutePath):
        userBaseRelativePath = self.convertAbsolutePathToBaseRelative(userAbsolutePath)
        self.setElementAttribute("yep:user", "src", userBaseRelativePath)

    def setUserContextDump(self, userContextDump):
        self.setElementContent("yep:userContextDump", userContextDump)


class LibertyAllianceAccountHolder(dataholders.XmlHolder):
    def deletePublicName(self):
        pass

    def getPublicName(self):
        publicName = self.getRootElement().getNameIdentifier()
        if publicName is None:
            return None
        # Remove "/" from name identifier (base64 includes the "/").
        publicName = strings.simplify(publicName)
        if self.isRootElder:
            publicName = os.path.join(self.getDirectoryAbsolutePath(), publicName)
        return publicName

    def setPublicName(self, publicName):
        pass

    publicName = property(getPublicName, setPublicName, deletePublicName)


elements.registerElement(
    namespaces.yep.uri, "libertyAllianceAccount", LibertyAllianceAccount,
    holderClass = LibertyAllianceAccountHolder)
