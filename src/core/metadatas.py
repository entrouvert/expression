# -*- coding: UTF-8 -*-


# Expression
# By: Frederic Peters <fpeters@entrouvert.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#
# Copyright (C) 2004 Entr'ouvert, Frederic Peters & Emmanuel Raviart
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


"""Metadatas Module"""


import dataholders
import elements
import environs
import faults
import libxml2
import logs
import namespaces
import things
import time

class Metadata(things.Thing):
    def deleteDublinCoreCreator(self):
        nodes = self.evaluationXpath("dc:creator")
        if nodes:
            node = nodes[0]
            node.unlinkNode()
            node.freeNode()

    def deleteDublinCoreDate(self):
        nodes = self.evaluateXpath("dc:date")
        if nodes:
            node = nodes[0]
            node.unlinkNode()
            node.freeNode()


    def deleteDublinCoreDescription(self):
        nodes = self.evaluateXpath("dc:description")
        if nodes:
            node = nodes[0]
            node.unlinkNode()
            node.freeNode()

    def deleteDublinCoreHasPart(self, xsiType, mimeType = None, id=None):
        
        xpathReq = 'dcterms:hasPart[@xsi:type="dcterms:%s"]' % xsiType 
        if(mimeType is not None):
            xpathReq += '[@yep:type="%s"]' % mimeType
        if(id is not None):
            xpathReq += '[@yep:id="%s"]' % id
        nodes = self.evaluateXpath(xpathReq)
        if nodes:
            node = nodes[0]
            node.unlinkNode()
            node.freeNode()


    def deleteDublinCoreSubject(self):
        nodes = self.evaluateXpath("dc:subject")
        if nodes:
            node = nodes[0]
            node.unlinkNode()
            node.freeNode()

    def deleteDublinCoreTitle(self):
        nodes = self.evaluateXpath("dc:title")
        if nodes:
            node = nodes[0]
            node.unlinkNode()
            node.freeNode()
    
    def getDublinCoreCreator(self):
        nodes = self.evaluateXpath("dc:creator")
        if nodes:
            return nodes[0].content
        else:
            return None
    
    def getDublinCoreDate(self):
        nodes = self.evaluateXpath("dc:date")
        if nodes:
            return nodes[0].content
        else:
            return None
    
    def getDublinCoreDescription(self):
        nodes = self.evaluateXpath("dc:description")
        if nodes:
            return nodes[0].content
        else:
            return None

    def getDublinCoreHasPart(self, xsiType, mimeType = None, id=None):
        xpathReq = 'dcterms:hasPart[@xsi:type="dcterms:%s"]' % xsiType 
        if(mimeType is not None):
            xpathReq += '[@yep:type="%s"]' % mimeType
        if(id is not None):
            xpathReq += '[@yep:id="%s"]' % id
        nodes = self.evaluateXpath(xpathReq)
        if nodes:
            return nodes[0].content
        else:
            return None

    def getDublinCoreSubject(self):
        nodes = self.evaluateXpath("dc:subject")
        if nodes:
            return nodes[0].content
        else:
            return None

    def getDublinCoreTitle(self):
        nodes = self.evaluateXpath("dc:title")
        if nodes:
            return nodes[0].content
        else:
            return None

    def setDublinCoreCreator(self, creator):
        self.setElementContent("dc:creator", creator)
    
    def setDublinCoreDate(self, localtime):
        assert type(localtime) is time.struct_time
        tz = (time.timezone-localtime[8]*3600)/3600
        tzs = '%c%02d:00' % (((tz < 0 and '+') or '-'), abs(tz))
        date = time.strftime('%Y-%m-%dT%H:%M:%S', localtime) + tzs
        self.setElementContent("dc:date", date)
    
    def setDublinCoreDescription(self, description):
        self.setElementContent("dc:description", description)
    
    def setDublinCoreHasPart(self, xsiType, reference, mimeType = None, id = None):
        try:
            ns = self.node.searchNs(self.node.doc, 'dcterms')
        except libxml2.treeError:
            ns = self.newNamespace(namespaces.getUri('dcterms'))
        ## - Create new generic hasPart node.
        node = self.node.newTextChild(ns, 'hasPart', reference)
        ## - Set xsiType and type properties.
        try:
            ns = self.node.searchNs(self.node.doc, 'xsi')
        except libxml2.treeError:
            ns = self.newNamespace(namespaces.getUri('xsi'))
        node.setProp('xsi:type','dcterms:%s' % xsiType)
        if mimeType:
            try:
                ns = self.node.searchNs(self.node.doc, 'yep')
            except libxml2.treeError:
                ns = self.newNamespace(namespaces.getUri('yep'))
            node.setProp('yep:type', mimeType)
        if id:
            try:
                ns = self.node.searchNs(self.node.doc, 'yep')
            except libxml2.treeError:
                ns = self.newNamespace(namespaces.getUri('yep'))
            node.setProp('yep:id', id)
        
        
    def setDublinCoreSubject(self, subject):
        self.setElementContent("dc:subject", subject)

    def setDublinCoreTitle(self, title):
        self.setElementContent("dc:title", title)


elements.registerElement(
    namespaces.yep.uri, "metadata", Metadata,
    "http://www.entrouvert.org/expression/schemas/Metadata.xsd",
    "http://www.entrouvert.org/expression/descriptions/Metadata.xml")
