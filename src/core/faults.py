# -*- coding: UTF-8 -*-


# Expression
# By: Frederic Peters <fpeters@entrouvert.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#
# Copyright (C) 2004 Entr'ouvert, Frederic Peters & Emmanuel Raviart
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


"""Faults Module"""


import environs


class AbstractFault(Exception):
    def __str__(self):
        return self.makeFaultString()

    def makeFaultString(self):
        raise NotImplementedError


class AbortFault(Exception):
    def makeFaultString(self):
        return "Abort"


class BadRequest(AbstractFault):
    reason = None
    station = None

    def __init__(self, reason = None, station = "current"):
        AbstractFault.__init__(self)
        if reason is not None:
            self.reason = reason
        if station == "current":
            station = environs.getVar("currentStation", default = None)
        self.station = station

    def makeFaultString(self):
        if self.path:
            return 'Bad request "%s" in %s' % (self.path, self.station)
        else:
            return 'Bad request in %s' % self.station


class PathForbidden(AbstractFault):
    path = None
    station = None

    def __init__(self, path, station = "current"):
        AbstractFault.__init__(self)
        self.path = path
        if station == "current":
            station = environs.getVar("currentStation", default = None)
        self.station = station

    def makeFaultString(self):
        return 'Access to path "%s" forbidden in %s' % (self.path, self.station)


class PathNotFound(AbstractFault):
    path = None
    station = None

    def __init__(self, path, station = "current"):
        AbstractFault.__init__(self)
        self.path = path
        if station == "current":
            station = environs.getVar("currentStation", default = None)
        self.station = station

    def makeFaultString(self):
        return 'Path "%s" not found in %s' % (self.path, self.station)


class PathUnauthorized(AbstractFault):
    path = None
    station = None

    def __init__(self, path, station = "current"):
        AbstractFault.__init__(self)
        self.path = path
        if station == "current":
            station = environs.getVar("currentStation", default = None)
        self.station = station

    def makeFaultString(self):
        return 'Access to path "%s" unauthorized in %s' % (self.path, self.station)
