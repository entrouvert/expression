# -*- coding: UTF-8 -*-


# Expression
# By: Frederic Peters <fpeters@entrouvert.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#
# Copyright (C) 2004 Entr'ouvert, Frederic Peters & Emmanuel Raviart
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


"""Things Module

A thing is a high level XML element, which can contain configuration elements like "users", etc.
"""


import elements
import logs


class Thing(elements.RootElement):
    def getAllConfigElementNodesAndOwnerCouples(self, xpath):
        relevantNodesAndOwnerCouples = super(
            Thing, self).getAllConfigElementNodesAndOwnerCouples(xpath)
        elementNodes = self.evaluateXpath("yep:element")
        for elementNode in elementNodes:
            nodes = self.evaluateXpath(xpath, contextNode = elementNode)
            if not nodes:
                continue
            nameNodes = self.evaluateXpath("@name", contextNode = elementNode)
            if not nameNodes:
                continue
            name = nameNodes[0].content
            namespaceUriNodes = self.evaluateXpath("@namespace", contextNode = elementNode)
            if not namespaceUriNodes:
                continue
            namespaceUri = namespaceUriNodes[0].content
            key = "%s / %s" % (namespaceUri, name)
            relevantNodesAndOwnerCouples[key] = (nodes, self)
        return relevantNodesAndOwnerCouples

    def getConfigDataHolderNodesAndOwner(self, mimeType, xpath, ignoreGeneralValue = False):
        dataHolderNodes = self.evaluateXpath("yep:inode[@mimeType = '%s']" %  mimeType.replace(
            "'", "&apos;"))
        if dataHolderNodes:
            dataHolderNode = dataHolderNodes[0]
            nodes = self.evaluateXpath(xpath, contextNode = dataHolderNode)
            if nodes:
                return nodes, self
        if not ignoreGeneralValue:
            nodes = self.evaluateXpath(xpath)
            if nodes:
                return nodes, self
        return super(Thing, self).getConfigDataHolderNodesAndOwner(
            mimeType, xpath, ignoreGeneralValue = ignoreGeneralValue)

    def getConfigElementNodesAndOwner(self, namespaceUri, name, xpath, ignoreGeneralValue = False):
        if namespaceUri is None:
            return [], self
        elementNodes = self.evaluateXpath("yep:element[@namespace = '%s' and @name = '%s']" % (
            namespaceUri.replace("'", "&apos;"), name.replace("'", "&apos;")))
        if elementNodes:
            elementNode = elementNodes[0]
            nodes = self.evaluateXpath(xpath, contextNode = elementNode)
            if nodes:
                return nodes, self
        if not ignoreGeneralValue:
            nodes = self.evaluateXpath(xpath)
            if nodes:
                return nodes, self
        return super(Thing, self).getConfigElementNodesAndOwner(
            namespaceUri, name, xpath, ignoreGeneralValue = ignoreGeneralValue)

    def getConfigStationNodesAndOwner(self, xpath):
        nodes = self.evaluateXpath(xpath)
        if nodes:
            return nodes, self
        return super(Thing, self).getConfigStationNodesAndOwner(xpath)
