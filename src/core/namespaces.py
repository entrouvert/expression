# -*- coding: UTF-8 -*-


# Expression
# By: Frederic Peters <fpeters@entrouvert.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#
# Copyright (C) 2004 Entr'ouvert, Frederic Peters & Emmanuel Raviart
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


"""XML Namespaces Module"""


_names = {}
_uris = {}
nsList = []
nsFromName = {}
nsFromUri = {}


class Namespace(object):
    name = None
    uri = None

    def __init__(self, name, uri):
        self.name = name
        self.uri = uri
        _names[uri] = name
        _uris[name] = uri
        nsList.append(self)
        nsFromName[name] = self
        nsFromUri[uri] = self


def getName(uri):
    return _names[uri]


def getUri(name):
    return _uris[name]


amedes = Namespace("amedes", "http://www.lesdeveloppementsdurables.org/namespaces/amedes/0.0")
dav = Namespace("dav", "DAV:")
dbxml = Namespace("dbxml", "http://www.sleepycat.com/2002/dbxml")
dc = Namespace("dc", "http://purl.org/dc/elements/1.1/")
dcterms = Namespace("dcterms", "http://purl.org/dc/terms/")
ev = Namespace("ev", "http://www.w3.org/2001/xml-events")
html = Namespace("html", "http://www.w3.org/1999/xhtml")
mail = Namespace("mail", "http://www.entrouvert.org/namespaces/email/0.0")
md = Namespace("md", "urn:liberty:metadata:2003-08")
ooo = Namespace("ooo", "http://openoffice.org/2000/office")
svg = Namespace("svg", "http://www.w3.org/2000/svg")
xforms = Namespace("xforms", "http://www.w3.org/2002/xforms")
xhtml2 = Namespace("xhtml2", "http://www.w3.org/2002/06/xhtml2")
xsd = Namespace("xsd", "http://www.w3.org/2001/XMLSchema")
xslt = Namespace("xslt", "http://www.w3.org/1999/XSL/Transform")
xsi = Namespace("xsi", "http://www.w3.org/2001/XMLSchema-instance")
yep = Namespace("yep", "http://www.entrouvert.org/namespaces/expression/0.0")

