# -*- coding: UTF-8 -*-


# Expression
# By: Frederic Peters <fpeters@entrouvert.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#
# Copyright (C) 2004 Entr'ouvert, Frederic Peters & Emmanuel Raviart
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


"""HTTP Statuses Module"""


import elements
import namespaces
import things


class AbstractHttpStatus(things.Thing):
    def deleteCode(self):
        nodes = self.evaluateXpath("yep:code")
        if nodes:
            node = nodes[0]
            node.unlinkNode()
            node.freeNode()

    def deleteMessage(self):
        nodes = self.evaluateXpath("yep:message")
        if nodes:
            node = nodes[0]
            node.unlinkNode()
            node.freeNode()

    def getCode(self):
        nodes = self.evaluateXpath("yep:code")
        if nodes:
            return int(nodes[0].content)
        else:
            return None

    def getMessage(self):
        nodes = self.evaluateXpath("yep:message")
        if nodes:
            return nodes[0].content
        else:
            return None

    def newInTemporaryDocument(cls, code, message):
        self = super(AbstractHttpStatus, cls).newInTemporaryDocument()
        self.code = code
        self.message = message
        return self
    newInTemporaryDocument = classmethod(newInTemporaryDocument)

    def setCode(self, code):
        self.setElementContent("yep:code", str(code))

    def setMessage(self, message):
        self.setElementContent("yep:message", message)

    code = property(getCode, setCode, deleteCode)
    message = property(getMessage, setMessage, deleteMessage)


class AbstractHttpStatusWithPath(AbstractHttpStatus):
    def deletePath(self):
        nodes = self.evaluateXpath("yep:path")
        if nodes:
            node = nodes[0]
            node.unlinkNode()
            node.freeNode()

    def getPath(self):
        nodes = self.evaluateXpath("yep:path")
        if nodes:
            return nodes[0].content
        else:
            return None

    def setPath(self, path):
        self.setElementContent("yep:path", path)

    path = property(getPath, setPath, deletePath)


class Http201(AbstractHttpStatusWithPath):
    def newInTemporaryDocument(cls, message, path):
        self = super(Http201, cls).newInTemporaryDocument(201, message)
        self.path = path
        return self
    newInTemporaryDocument = classmethod(newInTemporaryDocument)


class Http301(AbstractHttpStatus):
    def deleteLocation(self):
        nodes = self.evaluateXpath("yep:location")
        if nodes:
            node = nodes[0]
            node.unlinkNode()
            node.freeNode()

    def getLocation(self):
        nodes = self.evaluateXpath("yep:location")
        if nodes:
            return nodes[0].content
        else:
            return None

    def newInTemporaryDocument(cls, message, location):
        self = super(Http301, cls).newInTemporaryDocument(301, message)
        self.location = location
        return self
    newInTemporaryDocument = classmethod(newInTemporaryDocument)

    def setLocation(self, location):
        self.setElementContent("yep:location", location)

    location = property(getLocation, setLocation, deleteLocation)


class Http302(AbstractHttpStatus):
    def deleteLocation(self):
        nodes = self.evaluateXpath("yep:location")
        if nodes:
            node = nodes[0]
            node.unlinkNode()
            node.freeNode()

    def getLocation(self):
        nodes = self.evaluateXpath("yep:location")
        if nodes:
            return nodes[0].content
        else:
            return None

    def newInTemporaryDocument(cls, message, location):
        self = super(Http302, cls).newInTemporaryDocument(302, message)
        self.location = location
        return self
    newInTemporaryDocument = classmethod(newInTemporaryDocument)

    def setLocation(self, location):
        self.setElementContent("yep:location", location)

    location = property(getLocation, setLocation, deleteLocation)


class Http400(AbstractHttpStatus):
    def deleteReason(self):
        nodes = self.evaluateXpath("yep:reason")
        if nodes:
            node = nodes[0]
            node.unlinkNode()
            node.freeNode()

    def getReason(self):
        nodes = self.evaluateXpath("yep:reason")
        if nodes:
            return nodes[0].content
        else:
            return None

    def newInTemporaryDocument(cls, message, reason):
        self = super(Http400, cls).newInTemporaryDocument(400, message)
        self.reason = reason
        return self
    newInTemporaryDocument = classmethod(newInTemporaryDocument)

    def setReason(self, reason):
        self.setElementContent("yep:reason", reason)

    reason = property(getReason, setReason, deleteReason)


class Http401(AbstractHttpStatusWithPath):
    def newInTemporaryDocument(cls, message, path):
        self = super(Http401, cls).newInTemporaryDocument(401, message)
        self.path = path
        return self
    newInTemporaryDocument = classmethod(newInTemporaryDocument)


class Http403(AbstractHttpStatusWithPath):
    def newInTemporaryDocument(cls, message, path):
        self = super(Http403, cls).newInTemporaryDocument(403, message)
        self.path = path
        return self
    newInTemporaryDocument = classmethod(newInTemporaryDocument)


class Http404(AbstractHttpStatusWithPath):
    def newInTemporaryDocument(cls, message, path):
        self = super(Http404, cls).newInTemporaryDocument(404, message)
        self.path = path
        return self
    newInTemporaryDocument = classmethod(newInTemporaryDocument)


class Http500(AbstractHttpStatus):
    def newInTemporaryDocument(cls, message):
        self = super(Http500, cls).newInTemporaryDocument(500, message)
        return self
    newInTemporaryDocument = classmethod(newInTemporaryDocument)


elements.registerElement(
    namespaces.yep.uri, "http201", Http201,
    "http://www.entrouvert.org/expression/schemas/HttpStatus.xsd",
    "http://www.entrouvert.org/expression/descriptions/Http201.xml")
elements.registerElement(
    namespaces.yep.uri, "http301", Http301,
    "http://www.entrouvert.org/expression/schemas/HttpStatus.xsd",
    "http://www.entrouvert.org/expression/descriptions/Http301.xml")
elements.registerElement(
    namespaces.yep.uri, "http302", Http302,
    "http://www.entrouvert.org/expression/schemas/HttpStatus.xsd",
    "http://www.entrouvert.org/expression/descriptions/Http302.xml")
elements.registerElement(
    namespaces.yep.uri, "http400", Http400,
    "http://www.entrouvert.org/expression/schemas/HttpStatus.xsd",
    "http://www.entrouvert.org/expression/descriptions/Http400.xml")
elements.registerElement(
    namespaces.yep.uri, "http401", Http401,
    "http://www.entrouvert.org/expression/schemas/HttpStatus.xsd",
    "http://www.entrouvert.org/expression/descriptions/Http401.xml")
elements.registerElement(
    namespaces.yep.uri, "http403", Http403,
    "http://www.entrouvert.org/expression/schemas/HttpStatus.xsd",
    "http://www.entrouvert.org/expression/descriptions/Http403.xml")
elements.registerElement(
    namespaces.yep.uri, "http404", Http404,
    "http://www.entrouvert.org/expression/schemas/HttpStatus.xsd",
    "http://www.entrouvert.org/expression/descriptions/Http404.xml")
elements.registerElement(
    namespaces.yep.uri, "http500", Http500,
    "http://www.entrouvert.org/expression/schemas/HttpStatus.xsd",
    "http://www.entrouvert.org/expression/descriptions/Http500.xml")
