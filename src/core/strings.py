# -*- coding: UTF-8 -*-


# Expression
# By: Frederic Peters <fpeters@entrouvert.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#
# Copyright (C) 2004 Entr'ouvert, Frederic Peters & Emmanuel Raviart
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


"""String Functions

Function latin1_to_ascii from
http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/251871
"""


import random


xlate = {
    u'\N{ACUTE ACCENT}': "'",
    u'\N{BROKEN BAR}': '|',
    u'\N{CEDILLA}': '{cedilla}',
    u'\N{CENT SIGN}': '{cent}',
    u'\N{COPYRIGHT SIGN}': '{C}',
    u'\N{CURRENCY SIGN}': '{currency}',
    u'\N{DEGREE SIGN}': '{degrees}',
    u'\N{DIAERESIS}': '{umlaut}',
    u'\N{DIVISION SIGN}': '/',
    u'\N{FEMININE ORDINAL INDICATOR}': '{^a}',
    u'\N{INVERTED EXCLAMATION MARK}': '!',
    u'\N{INVERTED QUESTION MARK}': '?',
    u'\N{LATIN CAPITAL LETTER A WITH ACUTE}': 'A',
    u'\N{LATIN CAPITAL LETTER A WITH CIRCUMFLEX}': 'A',
    u'\N{LATIN CAPITAL LETTER A WITH DIAERESIS}': 'A',
    u'\N{LATIN CAPITAL LETTER A WITH GRAVE}': 'A',
    u'\N{LATIN CAPITAL LETTER A WITH RING ABOVE}': 'A',
    u'\N{LATIN CAPITAL LETTER A WITH TILDE}': 'A',
    u'\N{LATIN CAPITAL LETTER AE}': 'Ae',
    u'\N{LATIN CAPITAL LETTER C WITH CEDILLA}': 'C',
    u'\N{LATIN CAPITAL LETTER E WITH ACUTE}': 'E',
    u'\N{LATIN CAPITAL LETTER E WITH CIRCUMFLEX}': 'E',
    u'\N{LATIN CAPITAL LETTER E WITH DIAERESIS}': 'E',
    u'\N{LATIN CAPITAL LETTER E WITH GRAVE}': 'E',
    u'\N{LATIN CAPITAL LETTER ETH}': 'Th',
    u'\N{LATIN CAPITAL LETTER I WITH ACUTE}': 'I',
    u'\N{LATIN CAPITAL LETTER I WITH CIRCUMFLEX}': 'I',
    u'\N{LATIN CAPITAL LETTER I WITH DIAERESIS}': 'I',
    u'\N{LATIN CAPITAL LETTER I WITH GRAVE}': 'I',
    u'\N{LATIN CAPITAL LETTER N WITH TILDE}': 'N',
    u'\N{LATIN CAPITAL LETTER O WITH ACUTE}': 'O',
    u'\N{LATIN CAPITAL LETTER O WITH CIRCUMFLEX}': 'O',
    u'\N{LATIN CAPITAL LETTER O WITH DIAERESIS}': 'O',
    u'\N{LATIN CAPITAL LETTER O WITH GRAVE}': 'O',
    u'\N{LATIN CAPITAL LETTER O WITH STROKE}': 'O',
    u'\N{LATIN CAPITAL LETTER O WITH TILDE}': 'O',
    u'\N{LATIN CAPITAL LETTER THORN}': 'th',
    u'\N{LATIN CAPITAL LETTER U WITH ACUTE}': 'U',
    u'\N{LATIN CAPITAL LETTER U WITH CIRCUMFLEX}': 'U',
    u'\N{LATIN CAPITAL LETTER U WITH DIAERESIS}': 'U',
    u'\N{LATIN CAPITAL LETTER U WITH GRAVE}': 'U',
    u'\N{LATIN CAPITAL LETTER Y WITH ACUTE}': 'Y',
    u'\N{LATIN SMALL LETTER A WITH ACUTE}': 'a',
    u'\N{LATIN SMALL LETTER A WITH CIRCUMFLEX}': 'a',
    u'\N{LATIN SMALL LETTER A WITH DIAERESIS}': 'a',
    u'\N{LATIN SMALL LETTER A WITH GRAVE}': 'a',
    u'\N{LATIN SMALL LETTER A WITH RING ABOVE}': 'a',
    u'\N{LATIN SMALL LETTER A WITH TILDE}': 'a',
    u'\N{LATIN SMALL LETTER AE}': 'ae',
    u'\N{LATIN SMALL LETTER C WITH CEDILLA}': 'c',
    u'\N{LATIN SMALL LETTER E WITH ACUTE}': 'e',
    u'\N{LATIN SMALL LETTER E WITH CIRCUMFLEX}': 'e',
    u'\N{LATIN SMALL LETTER E WITH DIAERESIS}': 'e',
    u'\N{LATIN SMALL LETTER E WITH GRAVE}': 'e',
    u'\N{LATIN SMALL LETTER ETH}': 'th',
    u'\N{LATIN SMALL LETTER I WITH ACUTE}': 'i',
    u'\N{LATIN SMALL LETTER I WITH CIRCUMFLEX}': 'i',
    u'\N{LATIN SMALL LETTER I WITH DIAERESIS}': 'i',
    u'\N{LATIN SMALL LETTER I WITH GRAVE}': 'i',
    u'\N{LATIN SMALL LETTER N WITH TILDE}': 'n',
    u'\N{LATIN SMALL LETTER O WITH ACUTE}': 'o',
    u'\N{LATIN SMALL LETTER O WITH CIRCUMFLEX}': 'o',
    u'\N{LATIN SMALL LETTER O WITH DIAERESIS}': 'o',
    u'\N{LATIN SMALL LETTER O WITH GRAVE}': 'o',
    u'\N{LATIN SMALL LETTER O WITH STROKE}': 'o',
    u'\N{LATIN SMALL LETTER O WITH TILDE}': 'o',
    u'\N{LATIN SMALL LETTER SHARP S}': 'ss',
    u'\N{LATIN SMALL LETTER THORN}': 'th',
    u'\N{LATIN SMALL LETTER U WITH ACUTE}': 'u',
    u'\N{LATIN SMALL LETTER U WITH CIRCUMFLEX}': 'u',
    u'\N{LATIN SMALL LETTER U WITH DIAERESIS}': 'u',
    u'\N{LATIN SMALL LETTER U WITH GRAVE}': 'u',
    u'\N{LATIN SMALL LETTER Y WITH ACUTE}': 'y',
    u'\N{LATIN SMALL LETTER Y WITH DIAERESIS}': 'y',
    u'\N{LEFT-POINTING DOUBLE ANGLE QUOTATION MARK}': '<<',
    u'\N{MACRON}': '_',
    u'\N{MASCULINE ORDINAL INDICATOR}': '{^o}',
    u'\N{MICRO SIGN}': '{micro}',
    u'\N{MIDDLE DOT}': '*',
    u'\N{MULTIPLICATION SIGN}': '*',
    u'\N{NOT SIGN}': '{not}',
    u'\N{PILCROW SIGN}': '{paragraph}',
    u'\N{PLUS-MINUS SIGN}': '{+/-}',
    u'\N{POUND SIGN}': '{pound}',
    u'\N{REGISTERED SIGN}': '{R}',
    u'\N{RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK}': '>>',
    u'\N{SECTION SIGN}': '{section}',
    u'\N{SOFT HYPHEN}': '-',
    u'\N{SUPERSCRIPT ONE}': '{^1}',
    u'\N{SUPERSCRIPT THREE}': '{^3}',
    u'\N{SUPERSCRIPT TWO}': '{^2}',
    u'\N{VULGAR FRACTION ONE HALF}': '{1/2}',
    u'\N{VULGAR FRACTION ONE QUARTER}': '{1/4}',
    u'\N{VULGAR FRACTION THREE QUARTERS}': '{3/4}',
    u'\N{YEN SIGN}': '{yen}'
    }


def latin1_to_ascii (unicrap):
    """This takes a UNICODE string and replaces Latin-1 characters with
        something equivalent in 7-bit ASCII. It returns a plain ASCII string. 
        This function makes a best effort to convert Latin-1 characters into 
        ASCII equivalents. It does not just strip out the Latin-1 characters.
        All characters in the standard 7-bit ASCII range are preserved. 
        In the 8th bit range all the Latin-1 accented letters are converted 
        to unaccented equivalents. Most symbol characters are converted to 
        something meaningful. Anything not converted is deleted.
    """

    if not isinstance(unicrap, unicode):
        unicrap = unicode(unicrap, "UTF-8")
    r = ""
    for i in unicrap:
        if xlate.has_key(i):
            r += xlate[i]
        elif ord(i) >= 0x80:
            pass
        else:
            r += str(i)
    return r


def makePassword(
    length = 8,
    allowedChars = "ABCDEFGHJKLMNPQRSTUVWXYabcdefghijmnopqrstuvwxyz123456789"):
    """Generate a random password.

    @param length: length of the result string
    @type length: int
    @param allowedChars: characters allowed for the result string
    @type allowedChars: string
    """

    g = random.Random()
    return "".join([allowedChars[g.randrange(len(allowedChars))]
                    for i in range(length)])


def simplify(s):
    # We replace " " with "-" instead of "_" because this is better for Google indexing.
    # We also replace "." with "-" because of Expression current limitations for handling file name
    # extensions.
    # For HTTP authentication, a user id (ie a login) can not contain ":", because ":" is used as a
    # separator between login and password.
    s = latin1_to_ascii(s).lower().strip().replace(" ", "-").replace(
        "'", "").replace('"', "").replace(".", "-").replace("/", "-").replace(
        ":", "-")
    while True:
        t = s.replace("--", "-")
        if t == s:
            break
        s = t
    return s
