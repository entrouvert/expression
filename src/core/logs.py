# -*- coding: UTF-8 -*-


# Expression
# By: Frederic Peters <fpeters@entrouvert.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#
# Copyright (C) 2004 Entr'ouvert, Frederic Peters & Emmanuel Raviart
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


"""Logs Module"""


import environs


def critical(message, *arguments, **keywordArguments):
    logger = environs.getVar("logger", default = None)
    if logger is not None:
        logger.critical(message, *arguments, **keywordArguments)


def debug(message, *arguments, **keywordArguments):
    logger = environs.getVar("logger", default = None)
    if logger is not None:
        logger.debug(message, *arguments, **keywordArguments)


def error(message, *arguments, **keywordArguments):
    logger = environs.getVar("logger", default = None)
    if logger is not None:
        logger.error(message, *arguments, **keywordArguments)


def exception(message, *arguments):
    logger = environs.getVar("logger", default = None)
    if logger is not None:
        logger.exception(message, *arguments)


def info(message, *arguments, **keywordArguments):
    logger = environs.getVar("logger", default = None)
    if logger is not None:
        logger.info(message, *arguments, **keywordArguments)


def log(level, message, *arguments, **keywordArguments):
    logger = environs.getVar("logger", default = None)
    if logger is not None:
        logger.log(level, message, *arguments, **keywordArguments)


def warning(message, *arguments, **keywordArguments):
    logger = environs.getVar("logger", default = None)
    if logger is not None:
        logger.warning(message, *arguments, **keywordArguments)
