#!/usr/bin/env python2.4
# -*- coding: UTF-8 -*-


# Expression
# By: Frederic Peters <fpeters@entrouvert.com>
#     Emmanuel Raviart <eraviart@entrouvert.com>
#
# Copyright (C) 2004 Entr'ouvert, Frederic Peters & Emmanuel Raviart
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


"""Expression: Liberty Alliance enabled XML web server

Expression is a Python web server mainly dedicated to e-voting, e-forms and
self publishing. It is based on XML, XSLT, XML Schema, XPath, XForms, XMLSec,
SAML & Liberty Alliance standards.
"""


from distutils.core import setup
import sys
import os


classifiers = """\
Development Status :: 2 - Pre-Alpha
Environment :: Web Environment
Intended Audience :: Information Technology
License :: OSI Approved :: GNU General Public License (GPL)
Operating System :: POSIX :: Linux
Programming Language :: Python
Topic :: Internet :: WWW/HTTP :: Dynamic Content
Topic :: Internet :: WWW/HTTP :: HTTP Servers
"""

docLines = __doc__.split("\n")


def data_file_tree(destdir, sourcedir):
    r = []
    for root, dirs, files in os.walk(sourcedir):
        l = []
        for file in files:
            if file.endswith('~'):
                continue
            l.append(os.path.join(root, file))
        tuple = root.replace(sourcedir, destdir, 1), l
        r.append(tuple)
        if 'CVS' in dirs:
            dirs.remove('CVS')
    return r


# Check for Python >= 2.3
if sys.version_info < (2, 3, 0, '', 0):
    sys.exit("Error: Python 2.3 or newer is required. Current version is %s"
                % ".".join([str(x) for x in sys.version_info]))

setup(name = "expression",
      version = "0.4.0cvs20060305",
      maintainer = "Entr'ouvert",
      maintainer_email = "expression-devel@lists.labs.libre-entreprise.org",
      url = "http://www.entrouvert.org/expression",
      license = "http://www.fsf.org/licenses/gpl.html",
      platforms = ["GNU/Linux"],
      description = docLines[0],
      classifiers = [classifier for classifier in classifiers.split("\n") if classifier],
      long_description = "\n".join(docLines[2:]),
      package_dir = {"expression": "src"},
      packages = [
          "expression",
          "expression.core",
          "expression.modules",
          "expression.modules.parsers",
          "expression.modules.xforms",
          ],
      scripts = ["expression-server"],
      data_files = [
          #('/etc/expression', ['config.xml']),
          #('/etc/init.d', ['initscripts/expression']),
          ]
          + data_file_tree("xml/entities/expression", "xml/entities")
          + data_file_tree("xml/schema/expression", "xml/schema")
          + data_file_tree("xml/misc/expression", "xml/misc")
      )

